package com.example;

import java.security.Principal;

import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

	@Autowired
	KeycloakService keycloakService;

	@GetMapping(path = "/bearer/customers")
	public String showIndex(Principal principal, KeycloakSecurityContext context) {
//		String customers = keycloakService.getCustomers();
//		System.out.println(customers);
		//System.out.println(principal.getName());
		return "customers";
	}

}