package com.example;

import java.util.List;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class KeycloakServiceImpl implements KeycloakService {

	private static final String PASSWORD_RESET = "User password has been reset";
//	@Value("${keycloak.auth-server-url}")
//	private String baseUrl;
//	@Value("${keycloak.realm}")
//	private String realm;
//	@Value("${keycloak.resource}")
//	private String client;
//
//	@Value("${realm-admin-user}")
//	private String realmAdminUser;
//	@Value("${realm-admin-password}")
//	private String realmAdminPassword;

	@Override
	public String getCustomers() {
//		Keycloak instance = createKeycloakInstance(baseUrl, realm, null, null, client);
		return "list of customers";
	}
	

	private Keycloak createKeycloakInstance(String url, String realm, String user, String password, String client) {
		return Keycloak.getInstance(url, realm, "jdoe", "jdoe", client);
	}

	
}

