package com.example;

import java.util.List;

import org.keycloak.representations.idm.ClientRepresentation;
import org.springframework.stereotype.Service;

@Service
public interface KeycloakService {


	// list of clients in given realm
	public String getCustomers();

}
